# Export Metrics from gridftp transfer stats and gridftp under xinetd

[Mtail](https://github.com/google/mtail) program to export metrics from Gridftp transfer stats reports.

**NCSA Storage Enabling Technologies (SET) Group: set@ncsa.illinois.edu**

**Author: raila@illinois.edu**